package leson3.line.a.cod.dima.codalineleson3;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Admin on 20.02.2016.
 */
public class ActivityLayout2 extends Fragment implements InterfaceFragment  {
    static TextView textView = null;
    static String line = "";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.layout2, null);
        return view;
    }

    @Override
    public void onStart() {
        textView = (TextView) getActivity().findViewById(R.id.Layout2_TextView);
        line = getString(R.string.label_count);
        super.onStart();
    }

    @Override
    public void setCountChar(int length) {

        textView.setText(line+" = "+String.valueOf(length));
    }

}

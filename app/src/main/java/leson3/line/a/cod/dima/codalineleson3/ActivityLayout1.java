package leson3.line.a.cod.dima.codalineleson3;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

/**
 * Created by Admin on 20.02.2016.
 */
public class ActivityLayout1 extends Fragment {
    public static final String ID_VALUE = "TextEditValue";
    EditText editText = null;
    InterfaceFragment interfaceFragment = null;
    SharedPreferences resSP = null;

    @Override
    public void onStart() {
        super.onStart();
        interfaceFragment = new ActivityLayout2();
        resSP = this.getContext().getSharedPreferences("SharedPreferences",this.getContext().MODE_PRIVATE);
        editText = (EditText) getActivity().findViewById(R.id.Layout1_EditText);
        editText.setText(resSP.getString(ID_VALUE, ""));
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                interfaceFragment.setCountChar(editText.getText().toString().length());
            }

            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onPause() {
        if (resSP!= null) {
            resSP.edit().putString(ID_VALUE, editText.getText().toString()).apply();
        }
        super.onPause();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.layout1,null);
        return view;
    }
}
